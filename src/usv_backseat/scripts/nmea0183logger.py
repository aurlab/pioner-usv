#!/usr/bin/env python3
# license removed for brevity
# AURLab of NTNU
# Tore Mo-Bjørkelund 2021
# contact: tore.mo-bjorkelund@ntnu.no

import rospy
import numpy as np
import socket
from usv_backseat.msg import pioneer_gps_fix, pioneer_imu, pioneer_mod, manual_control
from std_msgs.msg import String

class NmeaLogger():
    def __init__(self):
        self.node_name = "NmeaInterface"
        rospy.init_node(self.node_name)

        self.frontseat_ip = "172.16.97.86"
        self.frontseat_udp_port = 2008
        self.frontseat_tcp_port = 2009
        self.sock = socket.socket() # UDP
        try:
            self.sock.connect((self.frontseat_ip,self.frontseat_tcp_port))
        except:
            print("Could not connect,try again")
            #TODO  rospy.signal shutdown

        self.rate = rospy.Rate(100)

        self.gps_pub_ = rospy.Publisher("/USV/Out/Gpsfix", pioneer_gps_fix, queue_size = 10)
        self.imu_pub_ = rospy.Publisher("/USV/Out/Imu", pioneer_imu, queue_size = 10)
        self.mod_pub_ = rospy.Publisher("/USV/Out/Mod", pioneer_mod, queue_size = 10)
        self.err_pub_ = rospy.Publisher("/USV/Out/Err", String, queue_size =10)
        self.ddt_pub_ = rospy.Publisher("/USV/Out/DevDataText", String, queue_size = 10)

        rospy.Subscriber("/USV/In/ManualControl", manual_control, self.ManualControlCB)

        self.sock.sendall(b'Hello, world')
        bfr = self.sock.recv(1024).decode("utf-8")
        bfr = bfr.split("$")
        for line in bfr:
            self.parseNMEA(line)


    def calc_NMEA_Checksum(self,sentence):
        checksum = 0
        for char in sentence:
            if char == "$":
                continue
            if char == "*":
                return checksum
            elif checksum == 0:
                checksum += ord(char)
            else:
                checksum = checksum ^ ord(char)
        return checksum

    def ManualControlCB(self,msg):
        norm = np.linalg.norm([msg.force_x.data,msg.torque_z.data])
        force_x = msg.force_x.data
        torque_z = msg.torque_z.data

        if norm >= 1.0:
            force_x = msg.force_x.data/norm
            torque_z = msg.torque_z.data/norm
        smsg = "$PMARMAN," + str(force_x) + ",0.0," + str(torque_z) + "*"
        checksum = self.calc_NMEA_Checksum(smsg)
        smsg += str(hex(checksum)).split("x")[1].upper() + "\r\n"
        print(smsg)
        print(smsg.encode('utf-8'))
        self.sock.send(smsg.encode())

    def spin(self):
        while not rospy.is_shutdown():
            bfr = self.sock.recv(1024).decode("utf-8")
            bfr = bfr.split("$")
            for line in bfr:
                self.parseNMEA(line)
            self.rate.sleep()

    def parseNMEA(self,line):
        #discard the checksum
        line = line.split("*")[0]
        #split into fields
        el = line.split(",")

        if el[0] ==  "PMARGPS":
            msg = pioneer_gps_fix()
            if len(el) == 9:
                #Allocate
                if el[1]:
                    msg.time.data = float(el[1])
                if el[2]:
                    msg.lat.data = float(el[2])
                if el[4]:
                    msg.lon.data = float(el[4])
                if el[6]:
                    msg.alt.data = float(el[6])
                if el[7]:
                    msg.sog.data = float(el[7])
                if el[8]:
                    msg.cog.data = float(el[8])
                self.gps_pub_.publish(msg)

        elif el[0] == "PMARIMU":
            msg = pioneer_imu()
            #Allocate
            if len(el) == 7:
                if el[1]:
                    msg.roll.data = float(el[1])
                if el[2]:
                    msg.pitch.data = float(el[2])
                if el[3]:
                    msg.yaw.data = float(el[3])
                if el[4]:
                    msg.p.data = float(el[4])
                if el[5]:
                    msg.q.data = float(el[5])
                if el[6]:
                    msg.r.data = float(el[6])
                self.imu_pub_.publish(msg)

        elif el[0] == "PMARMOD":
            msgm = pioneer_mod()
            if len(el)==3:
                msgm.mode.data = el[1]
                if el[2]:
                    el[2] = float(el[2])
                msgm.fuel_level.data = el[2]
                self.mod_pub_.publish(msgm)

        elif el[0] == "PMARERR":
            msge = String()
            if len(el) == 2:
                msge.data = el[1]
                self.err_pub_.publish(msge)

        else:
            msg = String()
            if line:
                msg.data = line
                self.ddt_pub_.publish(msg)


NL = NmeaLogger()
NL.spin()
