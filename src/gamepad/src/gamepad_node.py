#!/usr/bin/env python3

import rospy
import inputs
from std_msgs.msg import Int16
from std_msgs.msg import Float64
from usv_backseat.msg import manual_control
import numpy as np

class GamepadHandler(object):

    def __init__(self):

        # Ps. We need to change the manual_control function.

        # Initialize publisher for IMU
        #self.gamepad_pub = rospy.Publisher('gamepad', IMU, queue_size=10)
        self.x_button_pub = rospy.Publisher('X_button', Int16, queue_size=10)
        self.y_button_pub = rospy.Publisher('Y_button', Int16, queue_size=10)
        self.a_button_pub = rospy.Publisher('A_button', Int16, queue_size=10)
        self.b_button_pub = rospy.Publisher('B_button', Int16, queue_size=10)
        self.LX_joystick_pub = rospy.Publisher('LX_joystick', Float64, queue_size=10)
        self.LY_joystick_pub = rospy.Publisher('LY_joystick', Float64, queue_size=10)
        #self.RX_joystick_pub = rospy.Publisher('RX_joystick', Float64, queue_size=10)
        #self.RY_joystick_pub = rospy.Publisher('RY_joystick', Float64, queue_size=10)
        self.manual_control_pub = rospy.Publisher('manual_control', manual_control, queue_size=10)
        self.DPad_YAxis_pub = rospy.Publisher('DPad_YAxis', Int16, queue_size=10)
        self.DPad_XAxis_pub = rospy.Publisher('DPad_XAxis', Int16, queue_size=10)
        self.left_trigger_pub = rospy.Publisher('left_trigger', Int16, queue_size=10)
        self.right_trigger_pub = rospy.Publisher('right_trigger', Int16, queue_size=10)
        self.left_bumper_pub = rospy.Publisher('left_bumper', Int16, queue_size=10)
        self.right_bumper_pub = rospy.Publisher('right_bumper', Int16, queue_size=10)

        self.eventToFunctionMap = {
            "BTN_NORTH": self.handleXButton,
            "BTN_WEST": self.handleYButton,
            "BTN_EAST": self.handleBButton,
            "BTN_SOUTH": self.handleAButton,
            "ABS_X": self.handleLeftXAxis,
            "ABS_Y": self.handleLeftYAxis,
            "ABS_RX": self.handleRightXAxis,
            "ABS_RY": self.handleRightYAxis,
            "ABS_HAT0Y": self.handleDPadYAxis,
            "ABS_HAT0X": self.handleDPadXAxis,
            "BTN_TL": self.handleLeftTrigger,
            "BTN_TR": self.handleRightTrigger,
            "BTN_SELECT": self.handleLeftBumper,
            "BTN_START": self.handleRightBumper
        }

        self.x_pow = 0
        self.y_pow = 0

    def handleXButton(self, value):
        value_msg = Int16()
        value_msg.data = value
        self.x_button_pub.publish(value_msg)

    def handleYButton(self, value):
        value_msg = Int16()
        value_msg.data = value
        self.y_button_pub.publish(value_msg)

    def handleBButton(self, value):
        value_msg = Int16()
        value_msg.data = value
        self.b_button_pub.publish(value_msg)

    def handleAButton(self, value):
        value_msg = Int16()
        value_msg.data = value
        self.a_button_pub.publish(value_msg)

    def handleLeftTrigger(self, value):
        value_msg = Int16()
        value_msg.data = value
        self.left_trigger_pub.publish(value_msg)

    def handleRightTrigger(self, value):
        value_msg = Int16()
        value_msg.data = value
        self.right_trigger_pub.publish(value_msg)

    def handleLeftBumper(self, value):
        value_msg = Int16()
        value_msg.data = value
        self.left_bumper_pub.publish(value_msg)

    def handleRightBumper(self, value):
        value_msg = Int16()
        value_msg.data = value
        self.right_bumper_pub.publish(value_msg)


    def handleLeftXAxis(self, value):
        print("")
        value_msg = Float64()
        value_msg.data = self.filterAndNormalize(value)
        self.LX_joystick_pub.publish(value_msg)


    def handleLeftYAxis(self, value):
        print("")
        value_msg = Float64()
        value_msg.data = self.filterAndNormalize(value)
        self.LY_joystick_pub.publish(value_msg)

    def handleRightXAxis(self, value):
        print("")
        #value_msg = Float64()
        #value_msg.data = self.filterAndNormalize(value)
        #self.RX_joystick_pub.publish(value_msg)

    def handleRightYAxis(self, value):
        print("")
        #value_msg = Float64()
        #value_msg.data = self.filterAndNormalize(value)
        #self.RY_joystick_pub.publish(value_msg)

    def handleDPadYAxis(self, value):
        value_msg = Int16()
        value_msg.data = -value
        self.DPad_YAxis_pub.publish(value_msg)

    def handleDPadXAxis(self, value):
        value_msg = Int16()
        value_msg.data = -value
        self.DPad_XAxis_pub.publish(value_msg)

    def filterAndNormalize(self, value, lower=5000, upper=32768):
        """Normalizing the joystick axis range from (default) -32768<->32678 to -1<->1

        The sticks also tend to not stop at 0 when you let them go but rather some
        low value, so we'll filter those out as well.
        """
        if -lower < value < lower:
            return 0
        elif lower < value <= upper:
            return (value - lower) / (upper - lower)
        elif -upper <= value < -lower:
            return (value + lower) / (upper - lower)
        else:
            return 0

    def transformSteering(self):

        #pow_x = self.x_pow
        #pow_y = -self.y_pow

        #magnitude = np.sqrt((pow_x/(32768))**2 + (pow_y/(32768))**2) # between 0 and 1?
        #theta = np.arctan2(pow_x, pow_y) # between -pi and pi

        # All values should be in [-1, 1], but double check this!
        #force_vector = [magnitude * np.sign(pow_y), 0.0 , theta/np.pi]

        # Normalize with magnitude of vector if magnitude is larger than 1
        #if np.linalg.norm(force_vector) > 1.0:
            #force_vector = force_vector / np.linalg.norm(force_vector)

        manual_control_msg = manual_control()
        manual_control_msg.force_x = self.y_pow/32768
        manual_control_msg.force_y = 0.0
        manual_control_msg.torqe_z = sign(self.y_pow)*self.x_pow/32768
        self.manual_control_pub.publish(manual_control_msg)


        """
        theta = np.arctan2(pow_x, pow_y)
        if theta > -np.pi/2 and theta < np.pi /2:
            pioneer_r_pow = 1 + np.sin(-theta)
            pioneer_l_pow = 1 + np.sin(+theta)
            if pioneer_r_pow > 1:
                pioneer_r_pow = 1
            if pioneer_l_pow > 1:
                pioneer_l_pow = 1
        else:
            if theta < -np.pi/2:
                pioneer_r_pow = -1 - np.sin(-theta - np.pi)
                pioneer_l_pow = -1 - np.sin(+theta + np.pi)
                if pioneer_r_pow < -1:
                    pioneer_r_pow = -1
                if pioneer_l_pow < -1:
                    pioneer_l_pow = -1
            else:
                pioneer_r_pow = -1 - np.sin(-theta + np.pi)
                pioneer_l_pow = -1 - np.sin(+theta - np.pi)
                if pioneer_r_pow < -1:
                    pioneer_r_pow = -1
                if pioneer_l_pow > -1:
                    pioneer_l_pow = -1

        r = np.sqrt((pow_x/(32768))**2 + (pow_y/(32768))**2)
        if r > 1:
            r = 1
        """

        # TODO: Replace this!
        """
        manual_control_msg = manualControl()
        if r > 0.15:
            manual_control_msg.surge_right = pioneer_r_pow*r
            manual_control_msg.surge_left = pioneer_l_pow*r
        else:
            manual_control_msg.surge_right = 0.0
            manual_control_msg.surge_left = 0.0
        self.manual_control_pub.publish(manual_control_msg)
        """

    def spin(self):
        try:
            while not rospy.is_shutdown():
                self.spin_once()
        #except select.error:
        except rospy.ROSInterruptException:
            pass

    def spin_once(self):

        # Read gamepad
        events = inputs.get_gamepad()
        for event in events:
            print(event.code)
            if event.code == "ABS_RY": # Right joystick joystick y-axis
                self.y_pow = event.state
                print(self.y_pow)
            if event.code == "ABS_RX": # Right joystick joystick x-axis
                self.x_pow = event.state
                print(self.x_pow)
            if event.code == "ABS_X": # Left joystick x-axis
                self.eventToFunctionMap[event.code](event.state)
            if event.code == "ABS_Y": # Left joystick y-axis
                self.eventToFunctionMap[event.code](event.state)
            if event.code == "BTN_SOUTH": # A button
                self.eventToFunctionMap[event.code](event.state)
            if event.code == "BTN_WEST": # X button
                self.eventToFunctionMap[event.code](event.state)
            if event.code == "BTN_NORTH": # Y button
                self.eventToFunctionMap[event.code](event.state)
            if event.code == "BTN_EAST": # B button
                self.eventToFunctionMap[event.code](event.state)
            if event.code == "ABS_HAT0Y": # Dpad y-axis
                self.eventToFunctionMap[event.code](event.state)
            if event.code == "ABS_HAT0X": # Dpad x-axis
                self.eventToFunctionMap[event.code](event.state)
            if event.code == "BTN_TL": # Left trigger
                self.eventToFunctionMap[event.code](event.state)
            if event.code == "BTN_TR": # Left trigger
                self.eventToFunctionMap[event.code](event.state)
            if event.code == "BTN_SELECT": # Left bumper
                self.eventToFunctionMap[event.code](event.state)
            if event.code == "BTN_START": # Right bumper
                self.eventToFunctionMap[event.code](event.state)


        self.transformSteering()
        """
        try:
            self.transformSteering(x_pow, y_pow)
        except NameError:
            print("")
        else:
            print("")
        """

def main():
    '''Create a ROS node and instantiate the class.'''
    rospy.init_node('GamepadNode')
    gamepad_handler = GamepadHandler()
    gamepad_handler.spin()


if __name__ == '__main__':
    main()
