#!/usr/bin/env python3

# Consider making a

import rospy
import time
import os
import csv

from std_msgs.msg import Float64
from std_msgs.msg import String
from usv_backseat.msg import pioneer_gps_fix
from usv_backseat.msg import pioneer_imu
from usv_backseat.msg import pioneer_mod


class USVLogger(object):

    def __init__(self):

        # Create folder with time and date for log files
        time_now = time.strftime("%H_%M_%S")
        date_now = time.strftime("%d.%m.%Y")

        self.path = '/home/jensbremnes/usv_log_files/' + date_now + '/' + time_now
        rospy.loginfo(self.path)

        try:
            original_umask = os.umask(0)
            #os.makedirs(self.path)
            os.makedirs(self.path, mode=0o777, exist_ok=False) # was 0o777
        finally:
            os.umask(original_umask)

        # Topics we wish to log
        rospy.Subscriber("/USV/Out/Gpsfix", pioneer_gps_fix, self.gps_fix_callback)
        rospy.Subscriber("/USV/Out/Imu", pioneer_imu, self.imu_callback)
        rospy.Subscriber("/USV/Out/Mod", pioneer_mod, self.mod_callback)
        rospy.Subscriber("/USV/Out/Err", String, self.err_callback)
        rospy.Subscriber("/USV/Out/DevDataText", String, self.dev_data_text_callback)

    def gps_fix_callback(self, msg):
        filename = 'gps_fix.csv'

        # Correct for nanoseconds
        time = rospy.get_rostime()
        secs = str(time.secs)
        nsecs = str(time.nsecs)
        nsecs = nsecs.rjust(9, '0')

        if os.path.isfile(os.path.join(self.path, filename)):
            first = False
        else:
            first = True

        with open(os.path.join(self.path, filename), 'a') as csv_file:
            fieldnames = ['ROStime', 'time', 'lat', 'lon', 'alt', 'sog', 'cog']
            csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

            if first:
                csv_writer.writeheader()

            csv_writer.writerow({'ROStime': str(secs) + '.' + str(nsecs), \
            'time': str(msg.time), \
            'lat': str(msg.lat), \
            'lon': str(msg.lon), \
            'alt': str(msg.alt), \
            'sog': str(msg.sog), \
            'cog': str(msg.cog)})

        csv_file.close()

    def imu_callback(self, msg):
        filename = 'imu.csv'

        # Correct for nanoseconds
        time = rospy.get_rostime()
        secs = str(time.secs)
        nsecs = str(time.nsecs)
        nsecs = nsecs.rjust(9, '0')

        if os.path.isfile(os.path.join(self.path, filename)):
            first = False
        else:
            first = True

        with open(os.path.join(self.path, filename), 'a') as csv_file:
            fieldnames = ['ROStime', 'roll', 'pitch', 'yaw', 'p', 'q', 'r']
            csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

            if first:
                csv_writer.writeheader()

            csv_writer.writerow({'ROStime': str(secs) + '.' + str(nsecs), \
            'roll': str(msg.roll), \
            'pitch': str(msg.pitch), \
            'yaw': str(msg.yaw), \
            'p': str(msg.p), \
            'q': str(msg.q), \
            'r': str(msg.r)})

        csv_file.close()

    def mod_callback(self, msg):
        filename = 'mod.csv'

        # Correct for nanoseconds
        time = rospy.get_rostime()
        secs = str(time.secs)
        nsecs = str(time.nsecs)
        nsecs = nsecs.rjust(9, '0')

        if os.path.isfile(os.path.join(self.path, filename)):
            first = False
        else:
            first = True

        with open(os.path.join(self.path, filename), 'a') as csv_file:
            fieldnames = ['ROStime', 'mode', 'fuel_level']
            csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

            if first:
                csv_writer.writeheader()

            csv_writer.writerow({'ROStime': str(secs) + '.' + str(nsecs), \
            'mode': str(msg.mode), \
            'fuel_level': str(msg.fuel_level)})

        csv_file.close()

    def err_callback(self, msg):
        filename = 'err.csv'

        # Correct for nanoseconds
        time = rospy.get_rostime()
        secs = str(time.secs)
        nsecs = str(time.nsecs)
        nsecs = nsecs.rjust(9, '0')

        if os.path.isfile(os.path.join(self.path, filename)):
            first = False
        else:
            first = True

        with open(os.path.join(self.path, filename), 'a') as csv_file:
            fieldnames = ['ROStime', 'error']
            csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

            if first:
                csv_writer.writeheader()

            csv_writer.writerow({'ROStime': str(secs) + '.' + str(nsecs), \
            'error': str(msg.fuel_level)})

        csv_file.close()

    def dev_data_text_callback(self, msg):
        filename = 'dev_data_text.csv'

        # Correct for nanoseconds
        time = rospy.get_rostime()
        secs = str(time.secs)
        nsecs = str(time.nsecs)
        nsecs = nsecs.rjust(9, '0')

        if os.path.isfile(os.path.join(self.path, filename)):
            first = False
        else:
            first = True

        with open(os.path.join(self.path, filename), 'a') as csv_file:
            fieldnames = ['ROStime', 'dev_data_text']
            csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

            if first:
                csv_writer.writeheader()

            csv_writer.writerow({'ROStime': str(secs) + '.' + str(nsecs), \
            'dev_data_text': str(msg.dev_data_text)})

        csv_file.close()

def main():
    rospy.init_node('USVLoggerNode')
    usv_logger = USVLogger()
    rospy.spin()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
